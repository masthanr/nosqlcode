package com.novatium.toiNova.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.novatium.toiNova.model.StudentDetails;

//public interface StudentRepository<T> extends JpaRepository<StudentDetails,Integer>{
public interface StudentRepository<T> extends MongoRepository<StudentDetails,Integer>{

	StudentDetails findByContactNumberAndUserIdAndDeviceIdAndEmailId(String contactNumber, String userId, String deviceId,
			String emailId);

	StudentDetails findByUserIdAndDeviceIdAndPasswordAndTimestamp(String userId, String deviceId, String password,
			Date timestamp);

	StudentDetails findByemailIdAndDeviceIdAndTimestamp(String emailId, String deviceId, Date date);

	StudentDetails findByDeviceIdAndEmailIdAndTimestamp(String deviceId, String emailId, Date date);

	List<StudentDetails> findByclassName(String className);


}
