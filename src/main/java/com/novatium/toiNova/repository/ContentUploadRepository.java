package com.novatium.toiNova.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.novatium.toiNova.model.ContentUploadMaster;

public interface ContentUploadRepository extends MongoRepository<ContentUploadMaster,Integer>{

	

	    ContentUploadMaster findByClassIdAndSubjectId(String classId, String subjectId);

	    
	    

	
}
