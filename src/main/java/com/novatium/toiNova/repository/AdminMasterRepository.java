package com.novatium.toiNova.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;

//import org.springframework.data.jpa.repository.JpaRepository;

import com.novatium.toiNova.model.AdminDetails;

//public interface AdminMasterRepository extends JpaRepository<AdminDetails,Integer>{
	public interface AdminMasterRepository extends 	MongoRepository<AdminDetails,Integer>{


	AdminDetails findByMailId(String mail_id);

	AdminDetails findByUserName(String user_name);

	//sAdminDetails findByUserNameAndPassword(String userName, String password);

	AdminDetails findByUserNameAndPasswordAndMailId(String userName, String password, String emailId);

	AdminDetails findByLoginNameAndPassword(String userName, String password);

}
