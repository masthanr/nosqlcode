package com.novatium.toiNova.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.novatium.toiNova.model.OtpDetails;

public interface OtpRepository extends MongoRepository<OtpDetails,Integer>{

	OtpDetails findByEmailIdAndOtp(String email, String otp);
	

}
