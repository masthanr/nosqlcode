package com.novatium.toiNova.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection= "content_upload_master")
public class ContentUploadMaster implements java.io.Serializable {

	    private int contentUploadId;
	    private int contentCategoryId;
	    private String classId;
	    private String subjectId;
	    private String contentDisplayName;
	    private String contentFileName;
	    private String contentFilePath;
	    private String contentSize;
	    private Date createdTs;
	    private Date modifiedTs;
	    
	    
	    
	    public ContentUploadMaster() {
			
		}

		public ContentUploadMaster(int contentUploadId, int contentCategoryId, String classId, String subjectId,
				String contentDisplayName, String contentFileName, String contentFilePath, String contentSize,
				Date createdTs, Date modifiedTs) {
		
			this.contentUploadId = contentUploadId;
			this.contentCategoryId = contentCategoryId;
			this.classId = classId;
			this.subjectId = subjectId;
			this.contentDisplayName = contentDisplayName;
			this.contentFileName = contentFileName;
			this.contentFilePath = contentFilePath;
			this.contentSize = contentSize;
			this.createdTs = createdTs;
			this.modifiedTs = modifiedTs;
		}

		public String getClassId() {
	   	 return classId;
	    }

	    public void setClassId(String classId) {
	   	 this.classId = classId;
	    }

	    public String getSubjectId() {
	   	 return subjectId;
	    }

	    public void setSubjectId(String subjectId) {
	   	 this.subjectId = subjectId;
	    }

	    @Id
	    @SequenceGenerator(name="content_upload_id",sequenceName="content_upload_master_content_upload_id_seq", allocationSize=1)
	    @GeneratedValue(strategy=GenerationType.AUTO,generator="content_upload_id")
	    @Column(name = "content_upload_id", unique = true, nullable = false)
	    public int getContentUploadId() {
	   	 return this.contentUploadId;
	    }

	    public void setContentUploadId(int contentUploadId) {
	   	 this.contentUploadId = contentUploadId;
	    }

	    @Column(name = "content_category_id", nullable = false)
	    public int getContentCategoryId() {
	   	 return this.contentCategoryId;
	    }

	    public void setContentCategoryId(int contentCategoryId) {
	   	 this.contentCategoryId = contentCategoryId;
	    }

	    @Column(name = "content_display_name")
	    public String getContentDisplayName() {
	   	 return this.contentDisplayName;
	    }

	    public void setContentDisplayName(String contentDisplayName) {
	   	 this.contentDisplayName = contentDisplayName;
	    }

	    @Column(name = "content_file_name")
	    public String getContentFileName() {
	   	 return this.contentFileName;
	    }

	    public void setContentFileName(String contentFileName) {
	   	 this.contentFileName = contentFileName;
	    }

	    @Column(name = "content_file_path")
	    public String getContentFilePath() {
	   	 return this.contentFilePath;
	    }

	    public void setContentFilePath(String contentFilePath) {
	   	 this.contentFilePath = contentFilePath;
	    }

	    

	    @Column(name = "content_size")
	    public String getContentSize() {
	   	 return this.contentSize;
	    }

	    public void setContentSize(String contentSize) {
	   	 this.contentSize = contentSize;
	    }

	    @Temporal(TemporalType.TIMESTAMP)
	    @Column(name = "created_ts", nullable = false, length = 8)
	    public Date getCreatedTs() {
	   	 return this.createdTs;
	    }

	    public void setCreatedTs(Date createdTs) {
	   	 this.createdTs = createdTs;
	    }

	    @Temporal(TemporalType.TIMESTAMP)
	    @Column(name = "modified_ts", nullable = false, length = 8)
	    public Date getModifiedTs() {
	   	 return this.modifiedTs;
	    }

	    public void setModifiedTs(Date modifiedTs) {
	   	 this.modifiedTs = modifiedTs;
	    }

	    

}
