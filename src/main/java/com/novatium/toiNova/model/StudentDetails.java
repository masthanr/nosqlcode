package com.novatium.toiNova.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.mongodb.core.mapping.Document;

@SuppressWarnings("serial")

@Document(collection = "student_details")
public class StudentDetails  implements java.io.Serializable {

		private int stuId;
		private String name;
		private String gender;
	    private String className;
		private String currentInstitution;
		private String contactNumber;//
		private String emailId;//
		private String futureStream;
		private int age;
		private String deviceId;//
		private String userId;//
		private String location;
		private String password;
		private Date timestamp;
		private Date createdTs;
		private Date updatedTs;
		
		public StudentDetails() {
			
		}
		
		
	
		public StudentDetails(int stuId, String name, String gender, String className, String currentInstitution,
				String contactNumber, String emailId, String futureStream, int age, String deviceId, String userId,
				String location, String password, Date timestamp, Date createdTs, Date updatedTs) {
			
			this.stuId = stuId;
			this.name = name;
			this.gender = gender;
			this.className = className;
			this.currentInstitution = currentInstitution;
			this.contactNumber = contactNumber;
			this.emailId = emailId;
			this.futureStream = futureStream;
			this.age = age;
			this.deviceId = deviceId;
			this.userId = userId;
			this.location = location;
			this.password = password;
			this.timestamp = timestamp;
			this.createdTs = createdTs;
			this.updatedTs = updatedTs;
		}



		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		public int getStuId() {
			return stuId;
		}


		public void setStuId(int stuId) {
			this.stuId = stuId;
		}
		
		
		@Column(name="futureStream")
		public String getFutureStream() {
			return futureStream;
		}



		public void setFutureStream(String futureStream) {
			this.futureStream = futureStream;
		}



		@Column(name = "name", nullable = false)
		
		public String getName() {
			return name;
		}
		


		public void setName(String name) {
			this.name = name;
		}
		
		@Column(name="timestamp")
public Date getTimestamp() {
			return timestamp;
		}


		public void setTimestamp(Date timestamp) {
			this.timestamp = timestamp;
		}


@Column(name="createdTs")
		public Date getCreatedTs() {
			return createdTs;
		}

		public void setCreatedTs(Date createdTs) {
			this.createdTs = createdTs;
		}
		@Temporal(TemporalType.DATE)
		public Date getUpdatedTs() {
			return updatedTs;
		}

		public void setUpdatedTs(Date updatedTs) {
			this.updatedTs = updatedTs;
		}

		
		
		@Column(name = "gender")
		
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		@Column(name = "className")
		
		public String getClassName() {
			return className;
		}
		public void setClassName(String className) {
			this.className = className;
		}
		@Column(name = "currentInstitution")
		
		public String getCurrentInstitution() {
			return currentInstitution;
		}
		public void setCurrentInstitution(String currentInstitution) {
			this.currentInstitution = currentInstitution;
		}
		@Column(name = "contactNumber")
		public String getContactNumber() {
			return contactNumber;
		}


		public void setContactNumber(String contactNumber) {
			this.contactNumber = contactNumber;
		}
		
		@Column(name = "emailId")
		
		public String getEmailId() {
			return emailId;
		}
		public void setEmailId(String emailId) {
			this.emailId = emailId;
		}

		@Column(name = "userId",nullable = false)
		
		public String getUserId() {
			return userId;
		}


		public void setUserId(String userId) {
			this.userId = userId;
		}


		
		@Column(name = "age")
		
		public int getAge() {
			return age;
		}
		


		public void setAge(int age) {
			this.age = age;
			
		}
		@Column(name = "deviceid")
		
		public String getDeviceId() {
			return deviceId;
		}
		public void setDeviceId(String deviceId) {
			this.deviceId = deviceId;
		}
		
		
		
	
		
		@Column(name = "location")
		
		public String getLocation() {
			return location;
		}
		public void setLocation(String location) {
			this.location = location;
		}
		@Column(name = "password")
		
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
			
		}

		@Override
		public String toString() {
			return "StudentDetails [stud=" + stuId + ", name=" + name + ", gender=" + gender + ", className=" + className
					+ ", currentInstitution=" + currentInstitution + ", contactNumber=" + contactNumber + ", emailId="
					+ emailId + ", age=" + age + ", deviceId=" + deviceId + ", userId=" + userId + ", location="
					+ location + ", password=" + password + ", createdTs=" + createdTs + ", updatedTs=" + updatedTs
					+ "]";
		}
		


}
