package com.novatium.toiNova.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "admin_details")
public class AdminDetails {
	    private int id;
		private String userName;
		private String loginName;
		private String password;
		private String dob;
		private String mobileNo;
		private String gender;
		private String mailId;
		private String address;
		private String pincode;
		private String city;
		private String state;
		private Date createdTs;
	    private Date modifiedTs;
	    
	    
		public AdminDetails() {
			
		}
		public AdminDetails(int id, String userName, String loginName, String password, String dob, String mobileNo,
				String gender, String mailId, String address, String pincode, String city, String state, Date createdTs,
				Date modifiedTs) {
		this.id = id;
			this.userName = userName;
			this.loginName = loginName;
			this.password = password;
			this.dob = dob;
			this.mobileNo = mobileNo;
			this.gender = gender;
			this.mailId = mailId;
			this.address = address;
			this.pincode = pincode;
			this.city = city;
			this.state = state;
			this.createdTs = createdTs;
			this.modifiedTs = modifiedTs;
		}
		public Date getCreatedTs() {
	   	 return createdTs;
	    }
	    public void setCreatedTs(Date createdTs) {
	   	 this.createdTs = createdTs;
	    }
	    public Date getModifiedTs() {
	   	 return modifiedTs;
	    }
	    public void setModifiedTs(Date modifiedTs) {
	   	 this.modifiedTs = modifiedTs;
	    }
	    @Id
		@GeneratedValue(strategy = GenerationType.AUTO)
	    public int getId() {
	   	 return id;
	    }
	    public void setId(int id) {
	   	 this.id = id;
	    }
	    public String getUserName() {
	   	 return userName;
	    }
	    public void setUserName(String userName) {
	   	 this.userName = userName;
	    }
	    public String getPassword() {
	   	 return password;
	    }
	    public void setPassword(String password) {
	   	 this.password = password;
	    }
	    public String getDob() {
	   	 return dob;
	    }
	    public void setDob(String dob) {
	   	 this.dob = dob;
	    }
	    public String getMobileNo() {
	   	 return mobileNo;
	    }
	    public void setMobileNo(String mobileNo) {
	   	 this.mobileNo = mobileNo;
	    }
	    public String getGender() {
	   	 return gender;
	    }
	    public void setGender(String gender) {
	   	 this.gender = gender;
	    }
	    public String getMailId() {
	   	 return mailId;
	    }
	    public void setMailId(String mailId) {
	   	 this.mailId = mailId;
	    }
	    public String getAddress() {
	   	 return address;
	    }
	    public void setAddress(String address) {
	   	 this.address = address;
	    }
	    public String getPincode() {
	   	 return pincode;
	    }
	    public void setPincode(String pincode) {
	   	 this.pincode = pincode;
	    }
	    public String getCity() {
	   	 return city;
	    }
	    public void setCity(String city) {
	   	 this.city = city;
	    }
	    public String getState() {
	   	 return state;
	    }
	    public void setState(String state) {
	   	 this.state = state;
	    }
		public String getLoginName() {
			return loginName;
		}
		public void setLoginName(String loginName) {
			this.loginName = loginName;
		}
		@Override
		public String toString() {
			return "AdminDetails [id=" + id + ", userName=" + userName + ", loginName=" + loginName + ", password="
					+ password + ", dob=" + dob + ", mobileNo=" + mobileNo + ", gender=" + gender + ", mailId=" + mailId
					+ ", address=" + address + ", pincode=" + pincode + ", city=" + city + ", state=" + state
					+ ", createdTs=" + createdTs + ", modifiedTs=" + modifiedTs + "]";
		}
	    
	    
	    
	    
	    
	    

	}


