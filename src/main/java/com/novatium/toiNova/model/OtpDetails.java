package com.novatium.toiNova.model;
import java.util.Date;

import javax.persistence.*;

import org.springframework.data.mongodb.core.mapping.Document;




@Document(collection = "otp_details")
public class OtpDetails {
	
	    private int otpId;
	   private String otp;
	   private String devicedId;
	   private Date timestamp;
	   private Date createdTs;
	   private Date updatedTs;
	   private String emailId;
	   

	public OtpDetails() {
		
	}
	
	
	

	public OtpDetails(int otpId, String otp, String devicedId, Date timestamp, Date createdTs, Date updatedTs,
			String emailId) {
		
		this.otpId = otpId;
		this.otp = otp;
		this.devicedId = devicedId;
		this.timestamp = timestamp;
		this.createdTs = createdTs;
		this.updatedTs = updatedTs;
		this.emailId = emailId;
	}




	@Column(name="timestamp")
	//@Temporal(TemporalType.DATE)
	public Date getTimestamp() {
		return timestamp;
	}


	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}


	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getOtpId() {
		return otpId;
	}

	public void setOtpId(int otpId) {
		this.otpId = otpId;
	}
@Column(name="otp")
	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}
	@Column(name="emailId")
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}



	@Column(name="deviceId")
	public String getDevicedId() {
		return devicedId;
	}


	public void setDevicedId(String devicedId) {
		this.devicedId = devicedId;
	}
	@Column(name="createdTs")
	//@Temporal(TemporalType.DATE)
	public Date getCreatedTs() {
		return createdTs;
	}
	public void setCreatedTs(Date createdTs) {
		this.createdTs = createdTs;
	}
	@Column(name="updatedTs")
	//@Temporal(TemporalType.DATE)
	public Date getUpdatedTs() {
		return updatedTs;
	}
	public void setUpdatedTs(Date updatedTs) {
		this.updatedTs = updatedTs;
	}




	@Override
	public String toString() {
		return "OtpDetails [otpId=" + otpId + ", otp=" + otp + ", devicedId=" + devicedId + ", timestamp=" + timestamp
				+ ", createdTs=" + createdTs + ", updatedTs=" + updatedTs + ", emailId=" + emailId + "]";
	}
	
	
		}

