package com.novatium.toiNova.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.novatium.toiNova.model.AdminDetails;
import com.novatium.toiNova.model.ContentUploadMaster;
import com.novatium.toiNova.repository.AdminMasterRepository;
import com.novatium.toiNova.repository.ContentUploadRepository;
@Service
public class AdminService {
	  @Autowired
	    AdminMasterRepository adminMasterRepository;
	  @Autowired
	  ContentUploadRepository contentUploadRepository;
	    public Object adminRegistration(Map<String, Object> adminRegistrationData) {
	   	 Map<String, Object> result = new HashMap<String, Object>();
	   	AdminDetails admindata = null;   			 
	   	 if(adminRegistrationData != null )
	   	 {
	   		 String mail_id=(String)adminRegistrationData.get("mailId");
	   		AdminDetails mailCheck = adminMasterRepository.findByMailId(mail_id);
	   		 if(mailCheck == null)
	   		 {
	   			 String user_name=(String)adminRegistrationData.get("userName");
	   			AdminDetails userCheck = adminMasterRepository.findByUserName(user_name);
	   			 if(userCheck == null)
	   			 {
	   				 admindata = new AdminDetails();
	   				 admindata.setMailId(mail_id);
	   				 admindata.setUserName(user_name);
	   				 admindata.setLoginName((String)adminRegistrationData.get("loginName"));
	   				 admindata.setDob((String)adminRegistrationData.get("dob"));
	   				 admindata.setGender((String)adminRegistrationData.get("gender"));
	   				 admindata.setAddress((String)adminRegistrationData.get("address"));
	   				 admindata.setMobileNo((String)adminRegistrationData.get("mobileNo"));
	   				 admindata.setCity((String)adminRegistrationData.get("city"));
	   				 admindata.setState((String)adminRegistrationData.get("state"));
	   				 admindata.setPassword((String)adminRegistrationData.get("password"));
	   				 admindata.setPincode((String)adminRegistrationData.get("pincode"));
	   				 admindata.setCreatedTs(new Date());
	   				 admindata.setModifiedTs(new Date());
	   				 adminMasterRepository.save(admindata);    
	   				 result.put("Message", "Registered Successfully!!");
	   			 }
	   			 else
	   			 {
	   				 result.put("Message", "User Name Already Exist");
	   			 }   			 
	   		 }
	   		 else
	   		 {
	   			 result.put("Message", "Mail Id Already Exist");
	   		 }   				 
	   	 }
	   	 else
	   	 {
	   		 result.put("Message", "Data can't be null");
	   	 }    
	   			 
	   	 return result;
	    }

	    public Object adminLoginVerification(Map<String, Object> adminLoginVerification) {
	   	 Map<String, Object> result = new HashMap<String, Object>();
	   	AdminDetails admindata = null;
	   	 if(adminLoginVerification != null)
	   	 {
	   		 String loginName=(String)adminLoginVerification.get("loginName");
	   		 String password=(String)adminLoginVerification.get("password");
	   		AdminDetails userCheck = adminMasterRepository.findByLoginNameAndPassword(loginName,password);
	   		 if(userCheck != null)
	   		 {
	   			 result.put("Message", "LogIn Successfull!!!!");
	   			 result.put("Data", userCheck);
	   		 }
	   		 else
	   		 {
	   			 result.put("Message", "User Name and Password Doesn't Match");
	   		 }
	   		 
	   	 }
	   	 else
	   	 {
	   		 result.put("Message", "Data can't be null");
	   	 }
	   			 return result;
	    }

	    public Object editAdminDetails(Map<String, Object> editAdminDetails) {
	   	 Map<String, Object> result = new HashMap<String, Object>();
	   	AdminDetails admindata = null;
	   	 if(editAdminDetails != null)
	   	 {
	   		 String userName=(String)editAdminDetails.get("userName");
	   		 String password=(String)editAdminDetails.get("password");
	   		 String emailId=(String)editAdminDetails.get("mailId");
	   		AdminDetails userCheck = adminMasterRepository.findByUserNameAndPasswordAndMailId(userName,password,emailId);
	   		 if(userCheck != null)
	   		 {
	   			 admindata = new AdminDetails();
	   			 admindata.setDob((String)editAdminDetails.get("dob"));
	   			 admindata.setGender((String)editAdminDetails.get("gender"));
	   			 admindata.setAddress((String)editAdminDetails.get("address"));
	   			 admindata.setMobileNo((String)editAdminDetails.get("mobileNo"));
	   			 admindata.setCity((String)editAdminDetails.get("city"));
	   			 admindata.setState((String)editAdminDetails.get("state"));
	   			 admindata.setPincode((String)editAdminDetails.get("pincode"));
	   			 adminMasterRepository.save(admindata);
	   			 result.put("Message", "Updated Successfully!!");
	   		 
	   			 
	   		 }else {
	   			 result.put("Message", "Provide valid data");
	   		 }
	   		 
	   	 }
	   	 return result;
	    }

	    public Object contentUpload(Map<String, Object> contentData, MultipartFile contentfile) {
	      	 Map<String, Object> result = new HashMap<String, Object>();
	      	 int categoryId = (Integer)contentData.get("categoryId");
	      	 String classId = (String)contentData.get("classId");
	      	 String subjectName = (String)contentData.get("subjectName");
	      	 String contentDisplayName = (String)contentData.get("contentDisplayName");
	      	 String savePath = "";
	      	 String extraPath = classId + "/" + subjectName + "/";
	      	 String serverIP = "";
	      	 if (contentfile != null && !contentfile.isEmpty()) {

	      		 uploadFileHandler(contentfile, savePath + extraPath);
	      		 Date date = new Date();
	      		 String fileName = contentfile.getOriginalFilename().replace(" ", "_");
	      		 ContentUploadMaster contentUploadMaster = new ContentUploadMaster();
	      		 contentUploadMaster.setContentCategoryId(categoryId);
	      		 contentUploadMaster.setContentFileName(fileName);
	      		 contentUploadMaster.setContentDisplayName(contentDisplayName);
	      		 contentUploadMaster.setContentFilePath(serverIP + extraPath + fileName);
	      		 contentUploadMaster.setContentSize(contentfile.getSize() / 1024 + "KB");
	      		 contentUploadMaster.setCreatedTs(date);
	      		 contentUploadMaster.setModifiedTs(date);
	      		 contentUploadRepository.save(contentUploadMaster);
	      		 result.put("contentUploadId", contentUploadMaster.getContentUploadId());
	      		 result.put("contentFilePath", contentUploadMaster.getContentFilePath());
	      		 result.put("contentSize", contentUploadMaster.getContentSize());
	      		 result.put("Message", "Content Uploaded Successfully!");
	      		 return result;
	      		 
	      	 
	      	 }
	      	 else {
	      		 result.put("Message", "Content File is Empty");
	      		 return result;
	      	 }    
	       }
	       
	       public static String uploadFileHandler(MultipartFile file, String path) {

	      	 if (!file.isEmpty()) {
	      		 try {
	      			 byte[] bytes = file.getBytes();
	      			 File dir = new File(path);
	      			 if (!dir.exists())
	      				 dir.mkdirs();

	      			 String originalFilename = file.getOriginalFilename();
	      			 String fileName = originalFilename.replace(" ", "_");
	    
	      			 File serverFile = new File(dir.getAbsolutePath() + File.separator + fileName);
	      			 BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
	      			 stream.write(bytes);
	      			 stream.close();
	      		 } catch (Exception e) {
	      			 return "You failed to upload";
	      		 }
	      		 return "Success";
	      	 }
	      	 return null;
	       }

//	 
//	       public Object getUploadContents(String classId,String subjectId) throws NoSuchAlgorithmException {
//
//	      	 Map<String, Object> result = new HashMap<String, Object>();
//if(classId!=null && subjectId!=null) {
//	      	 List<Map<String, Object>> contentDetails = new ArrayList<Map<String, Object>>();
//	      	 Map<String, Object> contentDetail;
//	      	 List<Object[]> contentMasters = contentUploadRepository.findByClassIdAndSubjectId(classId,subjectId);
//	      	 if(contentMasters!=null) {
//	      	 for (Object[] contentdata : contentMasters) {
//	      		 contentDetail = new LinkedHashMap<String, Object>();
//	      		 contentDetail.put("contentUploadId", contentdata[0]);
//	      		 contentDetail.put("contentCategoryId", contentdata[1]);
//	      		 contentDetail.put("contentFileName", contentdata[5]);
//	      		 contentDetail.put("contentFilePath", contentdata[6]);
//	      		 contentDetail.put("contentSize", contentdata[7]);
//	      		 contentDetail.put("createdTs", contentdata[8]);
//	      		 contentDetail.put("contentDisplayName", contentdata[4]);
//	      		 contentDetails.add(contentDetail);
//	      	 }
//	      	 result.put("ContentData", contentDetails);
//	      	 }else {
//	      		result.put("Message", "No content available for this class"); 
//	      	 }
//}else {
//	result.put("Message", "Provide valid data");
//}
//	      	 return result;
//	       
//	       }
	       
	       
	       
	       
	       public Object getUploadContents(String classId,String subjectId) throws NoSuchAlgorithmException {
	    	   
	    	   	      	 Map<String, Object> result = new HashMap<String, Object>();
	    	   if(classId!=null && subjectId!=null) {
	           ContentUploadMaster contentUploadMaster = contentUploadRepository.findByClassIdAndSubjectId(classId,subjectId);
	           if (contentUploadMaster != null) {
	               result.put("ContentData", contentUploadMaster);
	              
	           }else {
	        	   result.put("Message", "No content available for this class");
	           }
	    	   }else {
	    			result.put("Message", "Provide valid data");
	    		 }
	           return  result;	       
	}
}


