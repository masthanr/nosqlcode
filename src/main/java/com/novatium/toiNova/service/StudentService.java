package com.novatium.toiNova.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.ResourceBundle;


import org.codehaus.jettison.json.JSONObject;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.novatium.toiNova.model.OtpDetails;
import com.novatium.toiNova.model.StudentDetails;
import com.novatium.toiNova.repository.OtpRepository;
import com.novatium.toiNova.repository.StudentRepository;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
@Service
public class StudentService<T> {
	private static final Logger LOGGER = Logger.getLogger(StudentService.class);
ResourceBundle bundle1=ResourceBundle.getBundle("application");
	@SuppressWarnings("rawtypes")
	@Autowired
	StudentRepository studentrepository;
	@Autowired
	OtpRepository otprepository;
	
	@SuppressWarnings("unused")
	public Object registerStudent(Map<String, Object> registerStudentData) {
		Map<String, Object> result = new HashMap<String, Object>();
		  Timestamp ts=new Timestamp((Long)registerStudentData.get("timeStamp"));  
          Date date=new Date(ts.getTime());  
		StudentDetails students=null;
		if(registerStudentData!=null) {
			StudentDetails stuDetails=studentrepository.findByContactNumberAndUserIdAndDeviceIdAndEmailId((String)registerStudentData.get("contactNumber"),(String)registerStudentData.get("userId"),
					(String)registerStudentData.get("deviceId"),(String)registerStudentData.get("emailId"));
			System.out.println(stuDetails);
			if(stuDetails==null) {
				students=new StudentDetails();
				students.setAge((Integer)registerStudentData.get("age"));
				students.setClassName((String)registerStudentData.get("className"));
			    students.setContactNumber((String)registerStudentData.get("contactNumber"));
			    students.setCreatedTs(new Date());
			    students.setFutureStream((String)registerStudentData.get("futureStream"));
			    students.setCurrentInstitution((String)registerStudentData.get("currentInstitution"));
			    students.setDeviceId((String)registerStudentData.get("deviceId"));
			    students.setEmailId((String)registerStudentData.get("emailId"));
			    students.setGender((String)registerStudentData.get("gender"));
			    students.setLocation((String)registerStudentData.get("gpsLocation"));
			    students.setName((String)registerStudentData.get("name"));
			    students.setPassword((String)registerStudentData.get("password"));
			    students.setTimestamp(date);
			    students.setUpdatedTs(new Date());
			    students.setUserId((String)registerStudentData.get("userId"));
			    studentrepository.save(students);
			    result.put("Message", "Students registered successfully");
			}else {
//				stuDetails.setLocation((String)registerStudentData.get("gpsLocation"));
//				stuDetails.setCurrentInstitution((String)registerStudentData.get("currentInstitution"));
//				stuDetails.setUpdatedTs(new Date());
//				stuDetails.setTimestamp(date);
//				studentrepository.save(stuDetails);
				result.put("Message", "This user already exist");
			}
		}else {
			result.put("Message", "provide valid data");
		}
		return result;
	}

	public Object studentLogin(Map<String, Object> loginStudentData) {
		Map<String, Object> result = new HashMap<String, Object>();
		 Timestamp ts=new Timestamp((Long)loginStudentData.get("timestamp"));  
         Date date=new Date(ts.getTime());
		if(loginStudentData!=null) {
			StudentDetails stuDetails=studentrepository.findByUserIdAndDeviceIdAndPasswordAndTimestamp((String)loginStudentData.get("userId"),(String)loginStudentData.get("deviceId"),
					(String)loginStudentData.get("password"),date);
			if(stuDetails!=null) {
				result.put("Message", "Login successfully");
			}else {
				result.put("Message", "enter valid login details");
			}
		
	}else {
		result.put("Message", "provide valid data");
	}

		return result;
	}

	public Object forgotPassword(Map<String, Object> studentPasswordData) {
		Map<String, Object> result = new HashMap<String, Object>();
		Map<String, Object> results = new HashMap<String, Object>();

		 Timestamp ts=new Timestamp((Long)studentPasswordData.get("timeStamp"));  
         Date date=new Date(ts.getTime());
		if(studentPasswordData!=null) {
			StudentDetails details=studentrepository.findByemailIdAndDeviceIdAndTimestamp((String)studentPasswordData.get("emailId"),(String)studentPasswordData.get("deviceId"),
					date);
			if(details!=null) {
				results.put("mailId", "sriram.m@novatium.com");
				
					 results.put("message", "Dear " + details.getName() + "," + " " + "yourPassword=" + details.getPassword());
					 sentEmail(results);

				result.put("Message","password send to your mail successfully");
				
			}else {
				result.put("Message", "No details available for this user");
			}
		}else {
			result.put("Message", "provide valid data");
		}
	
		return result;
	}

	public String sentEmail(Map<String, Object> obj) {
	   	 String ATNEmailResponse = "";

	   	 String serverUrl = bundle1.getString("global.ws.falconide");
	   	 String apikey = bundle1.getString("falconide.apikey");
	   	 String from = bundle1.getString("falconide.credentials.from");
	   	 String fromName = bundle1.getString("falconide.credentials.fromName");

	      	 try {
	      		 Client client = Client.create();

	      		 String final_url = serverUrl + "api_key=" + apikey + "&recipients=" +obj.get("mailId") +"  "+ "&content=" + obj.get("message")+ "&from=" + from + "&fromname="
	       				 + fromName ;

	      		 final_url = final_url.replace(" ", "%20");
	      		 com.sun.jersey.api.client.WebResource webResource = client.resource(final_url);
	      		 
	      		 ClientResponse response = webResource.type(javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED_TYPE)
	      				 .get(ClientResponse.class);

	      		 LOGGER.info("serverResponse : " + response.toString());
	      		 ATNEmailResponse = response.getEntity(String.class).toString().trim();
	      		 JSONObject json = new JSONObject(ATNEmailResponse);
	      		 String output = json.getString("message");
	      		 System.out.print("Reached Here");
	      		 return output;
	      		
	      	 } catch (Exception e) {
	      		 return "failure";
	      	 }
	       }

	
	
	
	public Object generateOtpEmail(Map<String, Object> emailOtpData) {
Map<String, Object> result = new HashMap<String, Object>();
Map<String, Object> data = new HashMap<String, Object>();
Timestamp ts=new Timestamp((Long)emailOtpData.get("timeStamp"));  
Date date=new Date(ts.getTime());
		OtpDetails otp=null;
		if(emailOtpData!=null) {
			StudentDetails detailsOtp=studentrepository.findByDeviceIdAndEmailIdAndTimestamp((String)emailOtpData.get("deviceId"),(String)emailOtpData.get("emailId"),
					date);
			System.out.println(detailsOtp);
			if(detailsOtp!=null) {
				Random rand = new Random();
				String otpStr = String.format("%06d", rand.nextInt(1000000));
				otp=new OtpDetails();
				
				otp.setCreatedTs(new Date());
				otp.setDevicedId((String)emailOtpData.get("deviceId"));
				otp.setEmailId((String)emailOtpData.get("emailId"));
				otp.setOtp(otpStr);
				otp.setTimestamp(date);
				otp.setUpdatedTs(new Date());
				otprepository.save(otp);
				data.put("mailId",(String)emailOtpData.get("emailId") );
				data.put("otp", otpStr);
				System.out.println(otpStr);
				sentOtpEmail(data);
				result.put("Message", "otp generated and send successfully");
			}else {
				result.put("Message", "provide valid details");
			}
		}
		return result;
	}
	public String sentOtpEmail(Map<String, Object> data) {
		String EmailResponse = "";

		 String serverUrl = bundle1.getString("global.ws.falconide");
	   	 String apikey = bundle1.getString("falconide.apikey");
	   	 String from = bundle1.getString("global.ws.falconide.from");
	   	 String fromName = bundle1.getString("falconide.credentials.fromName");
	 

	     		String final_url = null;

	    		try {
	    			Client client = Client.create();

	    			final_url = serverUrl + "api_key=" + apikey + "&from=" + from + "&fromname="
	    					+ fromName + "&recipients=" + data.get("mailId") + "&content="
	    					+ " your otp is ::::"+ data.get("otp");
System.out.println(final_url);
	    			final_url = final_url.replace(" ", "%20");
	    			WebResource webResource = client.resource(final_url);
	    			LOGGER.info("EMAIL url::" + final_url);
	    			ClientResponse response = webResource.type(javax.ws.rs.core.MediaType.APPLICATION_FORM_URLENCODED_TYPE)
	    					.get(ClientResponse.class);

	    			LOGGER.info("serverResponse : " + response.toString());
	    			EmailResponse = response.getEntity(String.class).toString().trim();
	    			JSONObject json = new JSONObject(EmailResponse);
	    			String output = json.getString("message");
	    			return output;
	    		} catch (Exception e) {
	    			e.printStackTrace();
	    			return "failure";
	    		}
	    	}

	
	

	//public Object checkOTPDetails(String email, String otp) {

		

	public Object checkOTPDetails(Map<String, Object> emailOtpData) {
		Map<String, Object> result = new HashMap<String, Object>();
		if(emailOtpData!=null) {
		OtpDetails otpdetails = otprepository.findByEmailIdAndOtp((String)emailOtpData.get("emailId"),(String)emailOtpData.get("otp"));
		if (otpdetails != null) {
			result.put("Message", "Success!! Entered OTP is Correct!!");
		} else {
			result.put("Message", "Failure!! Check OTP");
		}
		}else {
			result.put("Message","provide valid data");
		
	}		return result;
	}
	public Object getStudentDetails(String classId) {
		Map<String,Object> result=new HashMap<String,Object>();
		List<StudentDetails> resultList=new ArrayList<StudentDetails>();
if(classId!=null) {
		String className=classId;
		List<StudentDetails> details=studentrepository.findByclassName(className);
		if(details!=null) {
			for(StudentDetails stu:details) {
				resultList.add(stu);
			result.put("StudentData", resultList);
		}
		}else {
			result.put("Message","No students available for this class");
		}
}else {
	result.put("Message","Provide valid data");
}
		return result;
	}
}

