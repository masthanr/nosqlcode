package com.novatium.toiNova.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.novatium.toiNova.service.AdminService;
import com.novatium.toiNova.service.StudentService;

import static java.nio.charset.StandardCharsets.ISO_8859_1;
import static java.nio.charset.StandardCharsets.UTF_8;
@RestController
@RequestMapping(value="/student")
public class ToiController<T> {
	@Autowired
	StudentService<T> studentService;

@Autowired
AdminService adminService;
	@RequestMapping(value = "/registerStudent", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
		public @ResponseBody Object registerStudent(@RequestBody Map<String, Object> registerStudentData) {
			try {
				return studentService.registerStudent(registerStudentData);
			}catch(Exception e) {
				String errMsg="Error while register student";
				return errMsg;
			}
	}
	@RequestMapping(value = "/studentLogin", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public @ResponseBody Object studentLogin(@RequestBody Map<String, Object> loginStudentData) {
		try {
			return studentService.studentLogin(loginStudentData);
		}catch(Exception e) {
			String errMsg="Error while login ";
			return errMsg;
		}
	}
		@RequestMapping(value = "/forgotPassword", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
		public @ResponseBody Object forgotPassword(@RequestBody Map<String, Object> studentPasswordData) {
			try {
				return studentService.forgotPassword(studentPasswordData);
			}catch(Exception e) {
				String errMsg="Error while fetching student password";
				return errMsg;
			}
			
			
}
		@RequestMapping(value = "/otpemail", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
		public @ResponseBody Object generateOtpEmail(@RequestBody Map<String, Object> emailOtpData) {
			try {
				return studentService.generateOtpEmail(emailOtpData);
			}catch(Exception e) {
				String errMsg="Error while fetching student password";
				return errMsg;
			}
		}
		@RequestMapping(value = "/checkotpemail", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
		public @ResponseBody Object checkotpemail(@RequestBody Map<String, Object> emailOtpData) {
			try {
				return studentService.checkOTPDetails(emailOtpData);
			}catch(Exception e) {
				String errMsg="Error while fetching student password";
				return errMsg;
			}
		}

	    @RequestMapping(value = "/adminRegistration", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	    public @ResponseBody Object adminRegistration(@RequestBody Map<String, Object> adminRegistrationData) {

	   	 try {
	   		 return adminService.adminRegistration(adminRegistrationData);
	   	 } catch (Exception e) {
	   		 
	   		 return e;
	   	 }
	   	 
	    }
	    
	    @RequestMapping(value = "/adminLoginVerification", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	    public @ResponseBody Object adminLoginVerification(@RequestBody Map<String, Object> adminLoginVerification) {

	   	 try {
	   		 return adminService.adminLoginVerification(adminLoginVerification);
	   	 } catch (Exception e) {
	   		 
	   		 return e;
	   	 }
	   	 
	    }
	    
	    @RequestMapping(value = "/editAdminDetails", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	    public @ResponseBody Object editAdminDetails(@RequestBody Map<String, Object> editAdminDetails) {

	   	 try {
	   		 return adminService.editAdminDetails(editAdminDetails);
	   	 } catch (Exception e) {
	   		 
	   		 return e;
	   	 }
	   	 
	    }
	    @RequestMapping(value = "/getStudentDetails/{classId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	    public @ResponseBody Object getStudentDetails(@PathVariable String classId) {

	   	 try {
	   		 return studentService.getStudentDetails(classId);
	   	 } catch (Exception e) {
	   		 
	   		 return e;
	   	 }
	   	 
	    }

	    @RequestMapping(value = "/contentUpload", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	    public @ResponseBody Object contentUpload(@RequestParam(value = "data") String contentData,
	   		 @RequestParam(value = "file", required = false) MultipartFile Contentfile) {
	   	 byte[] ptext = contentData.getBytes(ISO_8859_1);
	   	 String contents = new String(ptext, UTF_8);
	   	 Map<String, Object> map = new HashMap<String, Object>();
	   	 try {
	   		 ObjectMapper mapper = new ObjectMapper();
	   		 map = mapper.readValue(contents, new TypeReference<Map<String, Object>>() {
	   		 });
	   		 return adminService.contentUpload(map, Contentfile);
	   	 } catch (Exception e) {
	   		 
	   		 return e;
	   	 }
	    }
	    
	    @RequestMapping(value = "/getUploadContents/{classId}/{subjectId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	    public @ResponseBody Object getCustomizeUploadContents(@PathVariable String classId,@PathVariable String subjectId) {

	   	 try {
	   		 return adminService.getUploadContents(classId,subjectId);
	   	 } catch (Exception e) {
	   		 
	   		 return e;
	   	 }
	    }
  

}
